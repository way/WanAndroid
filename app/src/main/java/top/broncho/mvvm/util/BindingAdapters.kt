package top.broncho.mvvm.util

import android.content.res.ColorStateList
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import top.broncho.gank.R
import org.jetbrains.anko.AnkoLogger

/**
 * DataBinding 的拓展适配器
 */
object BindingAdapters : AnkoLogger {


    /**
     * 本地图片加载
     */
    @BindingAdapter("imageUrl")
    @JvmStatic
    fun loadImage(view: ImageView, path: String?) {
        if (path.isNullOrBlank()) return
        view.loadImageCrop(path)
    }

    @BindingAdapter("duration")
    @JvmStatic
    fun duration(view: TextView, duration: Long) {
        var minute = duration.div(60)
        var second = duration.rem(60)
        val min = if (minute < 10) "0$minute" else "$minute"
        val sec = if (second < 10) "0$second" else "$second"
        view.text = "$min:$sec"
    }

    @BindingAdapter("collectColor")
    @JvmStatic
    fun collectColor(view: ImageView, isCollect: Boolean) {
//        val color =
//            view.context.getColor(if (isCollect) R.color.colorAccentExtra else R.color.colorTextSubtitle)
//        view.imageTintList = ColorStateList.valueOf(color)
    }


}

