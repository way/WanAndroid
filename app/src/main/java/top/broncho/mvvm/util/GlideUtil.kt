package top.broncho.mvvm.util

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions

fun ImageView.loadLocalImage(path: String) {
    val options = RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
    Glide.with(context.applicationContext).load(path).apply(options).into(this)
}

fun ImageView.loadImageCrop(
    url: String?
) {
    val options = RequestOptions()
        .centerCrop()
    Glide.with(context.applicationContext)
        .asBitmap()
        .load(url)
        .apply(options)
        .into(this)
}


fun ImageView.loadCircleImage(
    @DrawableRes
    resourceId: Int?
) {
    val options = RequestOptions()
        .centerCrop()
    Glide.with(context.applicationContext)
        .asBitmap()
        .load(resourceId)
        //.apply(options)
        .apply(RequestOptions.bitmapTransform(CircleCrop()))
        .into(this)
}