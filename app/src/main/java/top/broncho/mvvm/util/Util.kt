package top.broncho.mvvm.util

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.net.ConnectivityManager


fun Context.isWifiOpen(): Boolean {
    val cm =
        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val info = cm.activeNetworkInfo
    return info != null && info.isAvailable && info.isConnected
            && info.type == ConnectivityManager.TYPE_WIFI
}

fun Context.hasInternet(): Boolean {
    val cm =
        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val info = cm.activeNetworkInfo
    return info != null && info.isAvailable && info.isConnected
}

fun Context.copyString(text: String?): Boolean {
    if (text.isNullOrBlank()) {
        return false
    }
    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    clipboard.setPrimaryClip(ClipData.newPlainText(null, text))
    return true
}

