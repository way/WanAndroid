package top.broncho.mvvm.base

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.cxz.multiplestatusview.MultipleStatusView
import top.broncho.gank.constant.Constant
import top.broncho.gank.utils.Preference
import org.jetbrains.anko.AnkoLogger
import top.broncho.mvvm.di.Injectable
import javax.inject.Inject

abstract class BaseDiFragment<T : ViewDataBinding> : BaseFragment<T>(), AnkoLogger, Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    /**
     * check login
     */
    protected var isLogin: Boolean by Preference(Constant.LOGIN_KEY, false)

    /**
     * 缓存上一次的网络状态
     */
    protected var hasNetwork: Boolean by Preference(Constant.HAS_NETWORK_KEY, true)

    /**
     * 多种状态的 View 的切换
     */
    protected var mLayoutStatusView: MultipleStatusView? = null

}