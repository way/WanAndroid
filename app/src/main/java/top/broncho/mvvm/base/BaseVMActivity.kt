package top.broncho.mvvm.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.afollestad.materialdialogs.color.CircleView
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import org.jetbrains.anko.info
import reactivecircus.flowbinding.common.safeOffer
import top.broncho.gank.R
import top.broncho.gank.constant.Constant
import top.broncho.gank.utils.NetWorkUtil
import top.broncho.gank.utils.Preference
import top.broncho.gank.utils.SettingUtil
import top.broncho.gank.utils.StatusBarUtil
import javax.inject.Inject

abstract class BaseVMActivity<T : ViewDataBinding> : BaseDiActivity<T>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    /**
     * check login
     */
    protected var isLogin: Boolean by Preference(Constant.LOGIN_KEY, false)

    /**
     * 缓存上一次的网络状态
     */
    protected var hasNetwork: Boolean by Preference(Constant.HAS_NETWORK_KEY, true)

    /**
     * theme color
     */
    protected var mThemeColor: Int = SettingUtil.getColor()

    override fun onResume() {
        super.onResume()
        //initColor()
    }

    open fun initColor() {
        mThemeColor = if (!SettingUtil.getIsNightMode()) {
            SettingUtil.getColor()
        } else {
            resources.getColor(R.color.colorPrimary)
        }
        StatusBarUtil.setColor(this, mThemeColor, 0)
        if (this.supportActionBar != null) {
            this.supportActionBar?.setBackgroundDrawable(ColorDrawable(mThemeColor))
        }


        if (SettingUtil.getNavBar()) {
            window.navigationBarColor = CircleView.shiftColorDown(mThemeColor)
        } else {
            window.navigationBarColor = Color.BLACK
        }
    }

}