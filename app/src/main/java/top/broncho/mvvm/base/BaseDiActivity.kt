package top.broncho.mvvm.base

import androidx.databinding.ViewDataBinding
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject


abstract class BaseDiActivity<T : ViewDataBinding> : BaseActivity<T>(),
    HasAndroidInjector {
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector() = androidInjector

}