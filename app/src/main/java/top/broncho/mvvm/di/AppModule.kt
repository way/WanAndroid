package top.broncho.mvvm.di

import android.app.Application
import top.broncho.gank.db.AppDataBase
import top.broncho.gank.db.provideDataBase
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import top.broncho.mvvm.util.provideRetrofit
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
object AppModule {

    @Provides
    @Singleton
    fun provideDataBase(app: Application): AppDataBase = app.provideDataBase()

    @Provides
    @Singleton
    fun provideRetrofit(app: Application): Retrofit = app.provideRetrofit()
}