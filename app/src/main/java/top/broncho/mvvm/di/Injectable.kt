package top.broncho.mvvm.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable