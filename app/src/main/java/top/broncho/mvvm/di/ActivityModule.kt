package top.broncho.mvvm.di

import top.broncho.gank.ui.activity.*
import dagger.Module
import dagger.android.ContributesAndroidInjector
import top.broncho.gank.ui.setting.SettingActivity
import top.broncho.mvvm.di.annotation.ActivityScope


@Module
abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainFragmentModule::class])
    abstract fun contributeMainActivity(): top.broncho.gank.ui.main.MainActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSearchActivity(): SearchActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeRegisterActivity(): RegisterActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [CommonFragmentModule::class])
    abstract fun contributeCommonActivity(): CommonActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeRankActivity(): RankActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeScoreActivity(): ScoreActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeShareActivity(): ShareActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeContentActivity(): ContentActivity


    @ActivityScope
    @ContributesAndroidInjector(modules = [TodoFragmentModule::class])
    abstract fun contributeTodoActivity(): TodoActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [SettingFragmentModule::class])
    abstract fun contributeSettingActivity(): SettingActivity

}