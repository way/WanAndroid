package top.broncho.mvvm.di

import top.broncho.gank.ui.fragment.*
import dagger.Module
import dagger.android.ContributesAndroidInjector
import top.broncho.gank.ui.main.LauncherFragment
import top.broncho.gank.ui.main.rank.RankFragment
import top.broncho.gank.ui.main.score.ScoreFragment
import top.broncho.gank.ui.setting.SettingFragment

@Module
abstract class MainFragmentModule {

    //主要作用就是通过 @ContributesAndroidInjector  标记哪个类需要使用依赖注入功能
    //节省代码
    @ContributesAndroidInjector
    abstract fun contributeLauncherFragment(): LauncherFragment
    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeSquareFragment(): SquareFragment

    @ContributesAndroidInjector
    abstract fun contributeWeChatFragment(): WeChatFragment

    @ContributesAndroidInjector
    abstract fun contributeKnowledgeFragment(): KnowledgeFragment

    @ContributesAndroidInjector
    abstract fun contributeSystemFragment(): SystemFragment

    @ContributesAndroidInjector
    abstract fun contributeKnowledgeTreeFragment(): KnowledgeTreeFragment

    @ContributesAndroidInjector
    abstract fun contributeNavigationFragment(): NavigationFragment

    @ContributesAndroidInjector
    abstract fun contributeProjectFragment(): ProjectFragment

    @ContributesAndroidInjector
    abstract fun contributeProjectListFragment(): ProjectListFragment

    @ContributesAndroidInjector
    abstract fun contributeSearchListFragment(): SearchListFragment

    @ContributesAndroidInjector
    abstract fun contributeCollectFragment(): CollectFragment

    @ContributesAndroidInjector
    abstract fun contributeAddTodoFragment(): AddTodoFragment

    @ContributesAndroidInjector
    abstract fun contributeTodoFragment(): TodoFragment

    @ContributesAndroidInjector
    abstract fun contributeScoreFragment(): ScoreFragment

    @ContributesAndroidInjector
    abstract fun contributeRankFragment(): RankFragment
}

@Module
abstract class TodoFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeTodoFragment(): TodoFragment
}

@Module
abstract class SettingFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeSettingFragment(): SettingFragment
}

@Module
abstract class CommonFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeSearchListFragment(): SearchListFragment

    @ContributesAndroidInjector
    abstract fun contributeCollectFragment(): CollectFragment

    @ContributesAndroidInjector
    abstract fun contributeAddTodoFragment(): AddTodoFragment

    @ContributesAndroidInjector
    abstract fun contributeTodoFragment(): TodoFragment

    @ContributesAndroidInjector
    abstract fun contributeShareArticleFragment(): ShareArticleFragment

    @ContributesAndroidInjector
    abstract fun contributeAboutFragment(): AboutFragment

    @ContributesAndroidInjector
    abstract fun contributeQrCodeFragment(): QrCodeFragment
}