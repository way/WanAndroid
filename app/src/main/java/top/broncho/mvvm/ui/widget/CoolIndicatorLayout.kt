package top.broncho.mvvm.ui.widget

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.View
import top.broncho.gank.R
import com.just.agentweb.AgentWebUtils
import com.just.agentweb.BaseIndicatorView
import top.broncho.mvvm.ui.widget.CoolIndicator.Companion.create

class CoolIndicatorLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = -1
) : BaseIndicatorView(context, attrs, defStyleAttr) {
    private val coolIndicator: CoolIndicator by lazy { create((context as Activity)) }

    override fun show() {
        this.visibility = View.VISIBLE
        coolIndicator.start()
    }

    override fun setProgress(newProgress: Int) {}
    override fun hide() {
        coolIndicator.complete()
    }

    override fun offerLayoutParams(): LayoutParams {
        return LayoutParams(-1, AgentWebUtils.dp2px(context, 3f))
    }

    init {
        coolIndicator.progressDrawable = resources.getDrawable(
            R.drawable.default_drawable_indicator,
            context.theme
        )
        addView(coolIndicator, offerLayoutParams())
    }
}