package top.broncho.mvvm.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout

class BottomNavigationViewBehavior constructor(
    context: Context?,
    attrs: AttributeSet?
) : BaseBehavior(context, attrs) {
    override fun layoutDependsOn(
        parent: CoordinatorLayout,
        child: View,
        dependency: View
    ): Boolean {
        return true
    }

    override fun onDependentViewChanged(
        parent: CoordinatorLayout,
        child: View,
        dependency: View
    ): Boolean {
        if (canInit) {
            canInit = false
            animateHelper = TranslateAnimateHelper(child, MODE_BOTTOM)
        }
        return super.onDependentViewChanged(parent, child, dependency)
    }

    override fun onNestPreScrollInit(child: View?) {}
}