package top.broncho.mvvm.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout

class FloatingActionButtonBehavior constructor(
    context: Context?,
    attrs: AttributeSet?
) : BaseBehavior(context, attrs) {
    override fun onNestPreScrollInit(child: View?) {
    }

    override fun layoutDependsOn(
        parent: CoordinatorLayout,
        child: View,
        dependency: View
    ): Boolean {
        if (canInit) {
            animateHelper = ScaleAnimateHelper(child, STATE_HIDE)
            canInit = false
        }
        return super.layoutDependsOn(parent, child, dependency)
    }
}