package top.broncho.gank.webclient

import android.webkit.WebViewClient
import top.broncho.gank.utils.SettingUtil

/**
 * @author chenxz
 * @date 2019/11/24
 * @desc WebClientFactory
 */
object WebClientFactory {
    val WAN_ANDROID = "wanandroid.com"
    val JIAN_SHU = "https://www.jianshu.com"
    val JUE_JIN = "https://juejin.im/post/"
    val WEI_XIN = "https://mp.weixin.qq.com/s"
    val CSDN = "https://blog.csdn.net"

    fun create(url: String): WebViewClient {
        if (!SettingUtil.getIsNightMode()) {
            return BaseWebClient()
        }
        return when {
            url.contains(WAN_ANDROID) -> WanAndroidWebClient()
            url.startsWith(JIAN_SHU) -> JianShuWebClient()
            url.startsWith(JUE_JIN) -> JueJinWebClient()
            url.startsWith(WEI_XIN) -> WeiXinWebClient()
            url.startsWith(CSDN) -> CsdnWebViewClient()
            else -> BaseWebClient()
        }
    }

}