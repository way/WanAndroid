package top.broncho.gank.adapter

import android.app.ActivityOptions
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import top.broncho.gank.R
import top.broncho.gank.bean.Article
import top.broncho.gank.bean.NavigationBean
import top.broncho.gank.ui.activity.ContentActivity
import top.broncho.gank.utils.CommonUtil
import top.broncho.gank.utils.DisplayManager
import com.zhy.view.flowlayout.FlowLayout
import com.zhy.view.flowlayout.TagAdapter
import com.zhy.view.flowlayout.TagFlowLayout

/**
 * Created by chenxz on 2018/5/13.
 */
class NavigationAdapter(context: Context?, datas: MutableList<NavigationBean>) :
    BaseQuickAdapter<NavigationBean, BaseViewHolder>(R.layout.item_navigation_list, datas) {

    override fun convert(helper: BaseViewHolder?, item: NavigationBean?) {
        item ?: return
        helper?.setText(R.id.item_navigation_tv, item.name)
        val flowLayout: TagFlowLayout? = helper?.getView(R.id.item_navigation_flow_layout)
        val articles: List<Article> = item.articles
        flowLayout?.run {
            adapter = object : TagAdapter<Article>(articles) {
                override fun getView(parent: FlowLayout?, position: Int, article: Article?): View? {

                    val tv: TextView = LayoutInflater.from(parent?.context).inflate(
                        R.layout.flow_layout_tv,
                        flowLayout, false
                    ) as TextView

                    article ?: return null

                    val padding: Int = DisplayManager.dip2px(10F)
                    tv.setPadding(padding, padding, padding, padding)
                    tv.text = article.title
                    tv.setTextColor(CommonUtil.randomColor())

                    setOnTagClickListener { view, position, _ ->
                        val options: ActivityOptions = ActivityOptions.makeScaleUpAnimation(
                            view,
                            view.width / 2,
                            view.height / 2,
                            0,
                            0
                        )
                        val data: Article = articles[position]
                        ContentActivity.start(
                            context,
                            data.id,
                            data.title,
                            data.link,
                            options.toBundle()
                        )
                        true
                    }
                    return tv
                }
            }
        }
    }


}