package top.broncho.gank.adapter

import android.content.Context
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import top.broncho.gank.R
import top.broncho.gank.db.HistoryEntity

class SearchHistoryAdapter(private val context: Context?, datas: MutableList<HistoryEntity>) :
    BaseQuickAdapter<HistoryEntity, BaseViewHolder>(R.layout.item_search_history, datas) {

    override fun convert(helper: BaseViewHolder?, item: HistoryEntity?) {
        helper ?: return
        item ?: return

        helper.setText(R.id.tv_search_key, item.keyWord)
            .addOnClickListener(R.id.iv_clear)

    }
}