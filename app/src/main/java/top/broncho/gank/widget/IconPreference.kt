package top.broncho.gank.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import com.afollestad.materialdialogs.color.CircleView
import top.broncho.gank.R
import top.broncho.gank.utils.SettingUtil

/**
 * Created by chenxz on 2018/6/13.
 */
class IconPreference(context: Context, attrs: AttributeSet) : Preference(context, attrs) {

    private var circleImageView: CircleView? = null

    init {
        widgetLayoutResource = R.layout.item_icon_preference_preview
    }

    override fun onBindViewHolder(holder: PreferenceViewHolder?) {
        super.onBindViewHolder(holder)
        val color = SettingUtil.getColor()
        circleImageView = holder?.itemView?.findViewById(R.id.iv_preview)
        circleImageView?.setBackgroundColor(color)
    }

    fun setView() {
        val color = SettingUtil.getColor()
        circleImageView?.setBackgroundColor(color)
    }
}