package top.broncho.gank.db

import androidx.room.*
import androidx.room.OnConflictStrategy.IGNORE

@Dao
interface HistoryDao {
    @Insert(onConflict = IGNORE)
    suspend fun insert(entity: HistoryEntity): Long

    @Update
    suspend fun update(entity: HistoryEntity): Int

    @Delete
    suspend fun delete(entity: HistoryEntity): Int

    @Query("SELECT * FROM history")
    suspend fun getHistories(): MutableList<HistoryEntity>


    @Query("SELECT * FROM history WHERE keyWord=(:keyWord)")
    suspend fun getHistory(keyWord: String): HistoryEntity?

    @Query("UPDATE history SET _id=(:id) WHERE keyWord=(:keyWord)")
    suspend fun updateHistory(id: Long, keyWord: String): Int

    @Query("DELETE FROM history WHERE keyWord=(:keyWord)")
    suspend fun deleteHistory(keyWord: String): Int

    @Query("DELETE FROM history WHERE _id=(:id)")
    suspend fun deleteHistory(id: Long): Int

    @Query("DELETE FROM history")
    suspend fun deleteAll(): Int
}