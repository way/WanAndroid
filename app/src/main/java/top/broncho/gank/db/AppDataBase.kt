package top.broncho.gank.db

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

const val DATABASE_NAME = "history.db"

@Database(entities = [HistoryEntity::class], version = 1, exportSchema = true)
abstract class AppDataBase : RoomDatabase() {
    abstract fun historyDao(): HistoryDao
}

fun Application.provideDataBase() =
    Room.databaseBuilder(applicationContext, AppDataBase::class.java, DATABASE_NAME).build()
