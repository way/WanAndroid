package top.broncho.gank.db

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

const val TABLE_NAME = "History"

@Entity(tableName = TABLE_NAME, indices = [Index("_id", unique = true)])
class HistoryEntity(@PrimaryKey var _id: Long = System.currentTimeMillis(), val keyWord: String)