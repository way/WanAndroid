package top.broncho.gank.ui.fragment

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.WXChapterBean
import top.broncho.gank.http.exception.ErrorStatus
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class WeChatViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }

    val article = MutableLiveData<Result<MutableList<WXChapterBean>>>()

    fun getWXChapters() = launchOnlyResult(
        block = {
            api.getWXChapters()
        },
        success = {
            article.postValue(Result.success(it))
        },
        error = {
            article.postValue(Result.failure(it))
        }
    )

}