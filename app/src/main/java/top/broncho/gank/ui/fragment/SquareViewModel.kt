package top.broncho.gank.ui.fragment

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.ArticleResponseBody
import top.broncho.gank.http.exception.ErrorStatus
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class SquareViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }

    val article = MutableLiveData<Result<ArticleResponseBody>>()
    val collect = MutableLiveData<Result<Boolean>>()
    val cancel = MutableLiveData<Result<Boolean>>()

    fun getSquareList(num: Int) = launchOnlyResult(
        block = {
            api.getSquareList(num)
        },
        success = {
            article.postValue(Result.success(it))
        },
        error = {
            article.postValue(Result.failure(it))
        }
    )

    fun cancelCollectArticle(id: Int) = launchResult(
        block = {
            api.cancelCollectArticle(id)
        },
        success = {
            cancel.postValue(Result.success(true))
        },
        error = {
            cancel.postValue(Result.failure(it))
        }
    )

    fun addCollectArticle(id: Int) = launchResult(
        block = {
            api.addCollectArticle(id)
        },
        success = {
            collect.postValue(Result.success(true))
        },
        error = {
            collect.postValue(Result.failure(it))
        }
    )

}