package top.broncho.gank.ui.main.setting

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.tencent.bugly.beta.Beta
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import top.broncho.gank.BuildConfig
import top.broncho.gank.R
import top.broncho.gank.constant.Constant
import top.broncho.gank.ui.activity.ContentActivity
import top.broncho.gank.utils.CacheDataUtil


class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_setting, rootKey)
        val version = resources.getString(R.string.current_version, BuildConfig.VERSION_NAME)
        findPreference<Preference>("version")?.summary = version

        findPreference<Preference>("clearCache")?.summary =
            CacheDataUtil.getTotalCacheSize(requireContext())
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
        when (preference?.key) {
            "switch_show_top" -> {
            }
            "auto_nightMode" -> {
            }
            "color" -> {

            }
            "clearCache" -> {
                CacheDataUtil.clearAllCache(requireContext())
                requireContext().toast(getString(R.string.clear_cache_successfully))
                preference.summary = CacheDataUtil.getTotalCacheSize(requireContext())
            }
            "version" -> Beta.checkUpgrade()
            "official_website" ->
                requireContext().startActivity<ContentActivity>(
                    Pair(
                        Constant.CONTENT_URL_KEY,
                        getString(R.string.official_website_url)
                    )
                )
            "about_us" ->
                requireContext().startActivity<ContentActivity>(
                    Pair(
                        Constant.TYPE_KEY,
                        Constant.Type.ABOUT_US_TYPE_KEY
                    )
                )
            "changelog" ->
                requireContext().startActivity<ContentActivity>(
                    Pair(
                        Constant.CONTENT_URL_KEY,
                        getString(R.string.changelog_url)
                    )
                )
            "sourceCode" ->
                requireContext().startActivity<ContentActivity>(
                    Pair(
                        Constant.CONTENT_URL_KEY,
                        getString(R.string.source_code_url)
                    )
                )
            "copyRight" ->
                requireContext().alert(
                    messageResource = R.string.copyright_content,
                    titleResource = R.string.copyright
                ).apply {
                    isCancelable = false
                    positiveButton(android.R.string.ok) {
                        it.cancel()
                    }
                    show()
                }
            else -> {
            }
        }
        return super.onPreferenceTreeClick(preference)
    }
}
