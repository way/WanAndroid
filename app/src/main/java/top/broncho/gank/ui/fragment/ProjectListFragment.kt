package top.broncho.gank.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.chad.library.adapter.base.BaseQuickAdapter
import top.broncho.gank.R
import top.broncho.gank.adapter.ProjectAdapter
import top.broncho.gank.app.App
import top.broncho.gank.bean.Article
import top.broncho.gank.bean.ArticleResponseBody
import top.broncho.gank.constant.Constant
import top.broncho.gank.databinding.FragmentRefreshLayoutBinding
import top.broncho.gank.ext.showSnackMsg
import top.broncho.gank.ext.showToast
import top.broncho.gank.ui.activity.ContentActivity
import top.broncho.gank.ui.activity.LoginActivity
import top.broncho.gank.utils.NetWorkUtil
import top.broncho.gank.widget.SpaceItemDecoration
import kotlinx.android.synthetic.main.fragment_refresh_layout.*
import top.broncho.mvvm.base.BaseDiFragment

/**
 * Created by chenxz on 2018/5/20.
 */
class ProjectListFragment : BaseDiFragment<FragmentRefreshLayoutBinding>() {

    companion object {
        fun getInstance(cid: Int): ProjectListFragment {
            val fragment = ProjectListFragment()
            val args = Bundle()
            args.putInt(Constant.CONTENT_CID_KEY, cid)
            fragment.arguments = args
            return fragment
        }
    }

    private val viewModel by viewModels<ProjectListViewModel> { viewModelFactory }

    /**
     * 每页数据的个数
     */
    protected var pageSize = 20

    /**
     * 是否是下拉刷新
     */
    protected var isRefresh = true

    /**
     * LinearLayoutManager
     */
    protected val linearLayoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(requireContext())
    }

    /**
     * RecyclerView Divider
     */
    private val recyclerViewItemDecoration by lazy {
        SpaceItemDecoration(requireContext())
    }

    /**
     * RefreshListener
     */
    protected val onRefreshListener = SwipeRefreshLayout.OnRefreshListener {
        isRefresh = true
        onRefreshList()
    }

    /**
     * LoadMoreListener
     */
    protected val onRequestLoadMoreListener = BaseQuickAdapter.RequestLoadMoreListener {
        isRefresh = false
        swipeRefreshLayout.isRefreshing = false
        onLoadMoreList()
    }

    /**
     * cid
     */
    private var cid: Int = -1

    /**
     * Article datas
     */
    private val datas = mutableListOf<Article>()

    /**
     * ProjectAdapter
     */
    private val mAdapter: ProjectAdapter by lazy {
        ProjectAdapter(activity, datas)
    }

    fun hideLoading() {
        if (isRefresh) {
            mAdapter.setEnableLoadMore(true)
        }
    }

    fun showError(errorMsg: String) {
        if (isRefresh) {
            mAdapter.setEnableLoadMore(true)
        } else {
            mAdapter.loadMoreFail()
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_refresh_layout

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        viewModel.article.observe({ lifecycle }) {
            hideLoading()
            swipeRefreshLayout.isRefreshing = false
            if (it.isFailure) {
                showError(it.exceptionOrNull().toString())
                return@observe
            }
            setProjectList(it.getOrThrow())
        }

        viewModel.collect.observe({ lifecycle }) {
            showCollectSuccess(it.isSuccess)
        }
        viewModel.cancel.observe({ lifecycle }) {
            showCancelCollectSuccess(it.isSuccess)
        }
    }

    fun initView(view: View) {
        mLayoutStatusView = multiple_status_view
        swipeRefreshLayout.run {
            setOnRefreshListener(onRefreshListener)
        }
        recyclerView.run {
            layoutManager = linearLayoutManager
            adapter = mAdapter
            itemAnimator = DefaultItemAnimator()
            addItemDecoration(recyclerViewItemDecoration)
        }
        cid = arguments?.getInt(Constant.CONTENT_CID_KEY) ?: -1

        recyclerView.adapter = mAdapter

        mAdapter.run {
            setOnLoadMoreListener(onRequestLoadMoreListener, recyclerView)
            onItemClickListener = this@ProjectListFragment.onItemClickListener
            onItemChildClickListener = this@ProjectListFragment.onItemChildClickListener
            // setEmptyView(R.layout.fragment_empty_layout)
        }

    }

    override fun onResume() {
        super.onResume()
        if (viewModel.article.value == null) {
            lazyLoad()
        }
    }

    fun lazyLoad() {
        mLayoutStatusView?.showLoading()
        viewModel.requestProjectList(1, cid)
    }

    fun onRefreshList() {
        mAdapter.setEnableLoadMore(false)
        viewModel.requestProjectList(1, cid)
    }

    fun onLoadMoreList() {
        val page = mAdapter.data.size / pageSize + 1
        viewModel.requestProjectList(page, cid)
    }

    fun setProjectList(articles: ArticleResponseBody) {
        articles.datas.let {
            mAdapter.run {
                if (isRefresh) {
                    replaceData(it)
                } else {
                    addData(it)
                }
                pageSize = articles.size
                if (articles.over) {
                    loadMoreEnd(isRefresh)
                } else {
                    loadMoreComplete()
                }
            }
        }
        if (mAdapter.data.isEmpty()) {
            mLayoutStatusView?.showEmpty()
        } else {
            mLayoutStatusView?.showContent()
        }
    }

    fun scrollToTop() {
        recyclerView.run {
            if (linearLayoutManager.findFirstVisibleItemPosition() > 20) {
                scrollToPosition(0)
            } else {
                smoothScrollToPosition(0)
            }
        }
    }

    fun showCancelCollectSuccess(success: Boolean) {
        if (success) {
            showToast(getString(R.string.cancel_collect_success))
        }
    }

    fun showCollectSuccess(success: Boolean) {
        if (success) {
            showToast(getString(R.string.collect_success))
        }
    }

    /**
     * ItemClickListener
     */
    private val onItemClickListener = BaseQuickAdapter.OnItemClickListener { _, _, position ->
        if (datas.size != 0) {
            val data = datas[position]
            ContentActivity.start(activity, data.id, data.title, data.link)
        }
    }

    /**
     * ItemChildClickListener
     */
    private val onItemChildClickListener =
        BaseQuickAdapter.OnItemChildClickListener { _, view, position ->
            if (datas.size != 0) {
                val data = datas[position]
                when (view.id) {
                    R.id.item_project_list_like_iv -> {
                        if (isLogin) {
                            if (!NetWorkUtil.isNetworkAvailable(App.context)) {
                                showSnackMsg(resources.getString(R.string.no_network))
                                return@OnItemChildClickListener
                            }
                            val collect = data.collect
                            data.collect = !collect
                            mAdapter.setData(position, data)
                            if (collect) {
                                viewModel.cancelCollectArticle(data.id)
                            } else {
                                viewModel.addCollectArticle(data.id)
                            }
                        } else {
                            Intent(activity, LoginActivity::class.java).run {
                                startActivity(this)
                            }
                            showToast(resources.getString(R.string.login_tint))
                        }
                    }
                }
            }
        }

}