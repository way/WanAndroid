package top.broncho.gank.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import cn.bingoogolapple.bgabanner.BGABanner
import com.chad.library.adapter.base.BaseQuickAdapter
import top.broncho.gank.R
import top.broncho.gank.adapter.HomeAdapter
import top.broncho.gank.app.App
import top.broncho.gank.bean.Article
import top.broncho.gank.bean.ArticleResponseBody
import top.broncho.gank.bean.Banner
import top.broncho.gank.databinding.FragmentRefreshLayoutBinding
import top.broncho.gank.ext.showSnackMsg
import top.broncho.gank.ext.showToast
import top.broncho.gank.ui.activity.ContentActivity
import top.broncho.gank.ui.activity.LoginActivity
import top.broncho.gank.utils.ImageLoader
import top.broncho.gank.utils.NetWorkUtil
import top.broncho.gank.widget.SpaceItemDecoration
import kotlinx.android.synthetic.main.fragment_refresh_layout.*
import kotlinx.android.synthetic.main.item_home_banner.view.*
import org.jetbrains.anko.info
import top.broncho.mvvm.base.BaseDiFragment

/**
 * Created by chenxz on 2018/4/22.
 */
class HomeFragment : BaseDiFragment<FragmentRefreshLayoutBinding>() {

    companion object {
        fun getInstance(): HomeFragment = HomeFragment()
    }

    private val viewModel by viewModels<HomeViewModel> { viewModelFactory }

    /**
     * datas
     */
    private val datas = mutableListOf<Article>()

    /**
     * banner datas
     */
    private lateinit var bannerDatas: ArrayList<Banner>

    /**
     * banner view
     */
    private var bannerView: View? = null

    /**
     * RecyclerView Divider
     */
    private val recyclerViewItemDecoration by lazy {
        activity?.let {
            SpaceItemDecoration(it)
        }
    }

    /**
     * Home Adapter
     */
    private val homeAdapter: HomeAdapter by lazy {
        HomeAdapter(activity, datas)
    }

    /**
     * Banner Adapter
     */
    private val bannerAdapter: BGABanner.Adapter<ImageView, String> by lazy {
        BGABanner.Adapter<ImageView, String> { bgaBanner, imageView, feedImageUrl, position ->
            ImageLoader.load(activity, feedImageUrl, imageView)
        }
    }

    /**
     * LinearLayoutManager
     */
    private val linearLayoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(activity)
    }

    /**
     * is Refresh
     */
    private var isRefresh = true

    override fun getLayoutId(): Int = R.layout.fragment_refresh_layout

    fun scrollToTop() {
        recyclerView.run {
            if (linearLayoutManager.findFirstVisibleItemPosition() > 20) {
                scrollToPosition(0)
            } else {
                smoothScrollToPosition(0)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        viewModel.article.observe({ lifecycle }) {
            hideLoading()
            if (it.isFailure) {
                showError(it.exceptionOrNull().toString())
                return@observe
            }
            setArticles(it.getOrThrow())
        }
        viewModel.banner.observe({ lifecycle }) {
            hideLoading()
            if (it.isFailure) {
                showError(it.exceptionOrNull().toString())
                return@observe
            }
            setBanner(it.getOrThrow())
        }
        viewModel.collect.observe({ lifecycle }) {
            info { "collect: $it" }
            showCollectSuccess(it.isSuccess)
        }
        viewModel.cancel.observe({ lifecycle }) {
            info { "cancel: $it" }
            showCancelCollectSuccess(it.isSuccess)
        }
    }

    fun initView(view: View) {
        mLayoutStatusView = multiple_status_view
        swipeRefreshLayout.run {
            setOnRefreshListener(onRefreshListener)
        }
        recyclerView.run {
            adapter = homeAdapter
            itemAnimator = DefaultItemAnimator()
            recyclerViewItemDecoration?.let { addItemDecoration(it) }
        }

        bannerView = layoutInflater.inflate(R.layout.item_home_banner, null)
        bannerView?.banner?.run {
            setDelegate(bannerDelegate)
        }

        homeAdapter.run {
            //bindToRecyclerView(recyclerView)
            setOnLoadMoreListener(onRequestLoadMoreListener, recyclerView)
            onItemClickListener = this@HomeFragment.onItemClickListener
            onItemChildClickListener = this@HomeFragment.onItemChildClickListener
            // setEmptyView(R.layout.fragment_empty_layout)
            addHeaderView(bannerView)
        }
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.banner.value == null) {
            lazyLoad()
        }
    }

    fun lazyLoad() {
        mLayoutStatusView?.showLoading()
        viewModel.requestHomeData()
    }

    fun showLoading() {
        // swipeRefreshLayout.isRefreshing = isRefresh
    }

    fun hideLoading() {
        swipeRefreshLayout?.isRefreshing = false
        if (isRefresh) {
            homeAdapter.run {
                setEnableLoadMore(true)
            }
        }
    }

    fun showError(errorMsg: String) {
        mLayoutStatusView?.showError()
        homeAdapter.run {
            if (isRefresh)
                setEnableLoadMore(true)
            else
                loadMoreFail()
        }
    }

    fun setBanner(banners: List<Banner>) {
        bannerDatas = banners as ArrayList<Banner>
        val bannerFeedList = ArrayList<String>()
        val bannerTitleList = ArrayList<String>()
        banners.forEach {
            bannerFeedList.add(it.imagePath)
            bannerTitleList.add(it.title)
        }
        bannerView?.banner?.run {
            setAutoPlayAble(bannerFeedList.size > 1)
            setData(bannerFeedList, bannerTitleList)
            setAdapter(bannerAdapter)
        }
    }

    fun setArticles(articles: ArticleResponseBody) {
        articles.datas.let {
            homeAdapter.run {
                if (isRefresh) {
                    replaceData(it)
                } else {
                    addData(it)
                }
                val size = it.size
                if (size < articles.size) {
                    loadMoreEnd(isRefresh)
                } else {
                    loadMoreComplete()
                }
            }
        }
        if (homeAdapter.data.isEmpty()) {
            mLayoutStatusView?.showEmpty()
        } else {
            mLayoutStatusView?.showContent()
        }
    }

    fun showCollectSuccess(success: Boolean) {
        if (success) {
            showToast(resources.getString(R.string.collect_success))
        }
    }

    fun showCancelCollectSuccess(success: Boolean) {
        if (success) {
            showToast(resources.getString(R.string.cancel_collect_success))
        }
    }

    /**
     * RefreshListener
     */
    private val onRefreshListener = SwipeRefreshLayout.OnRefreshListener {
        isRefresh = true
        homeAdapter.setEnableLoadMore(false)
        viewModel.requestHomeData()
    }

    /**
     * LoadMoreListener
     */
    private val onRequestLoadMoreListener = BaseQuickAdapter.RequestLoadMoreListener {
        isRefresh = false
        swipeRefreshLayout.isRefreshing = false
        val page = homeAdapter.data.size / 20
        viewModel.requestArticles(page)
    }

    /**
     * ItemClickListener
     */
    private val onItemClickListener = BaseQuickAdapter.OnItemClickListener { _, _, position ->
        if (datas.size != 0) {
            val data = datas[position]
            ContentActivity.start(activity, data.id, data.title, data.link)
        }
    }

    /**
     * BannerClickListener
     */
    private val bannerDelegate =
        BGABanner.Delegate<ImageView, String> { banner, imageView, model, position ->
            if (bannerDatas.size > 0) {
                val data = bannerDatas[position]
                ContentActivity.start(activity, data.id, data.title, data.url)
            }
        }

    /**
     * ItemChildClickListener
     */
    private val onItemChildClickListener =
        BaseQuickAdapter.OnItemChildClickListener { _, view, position ->
            if (datas.size != 0) {
                val data = datas[position]
                when (view.id) {
                    R.id.iv_like -> {
                        if (isLogin) {
                            if (!NetWorkUtil.isNetworkAvailable(App.context)) {
                                showSnackMsg(resources.getString(R.string.no_network))
                                return@OnItemChildClickListener
                            }
                            val collect = data.collect
                            data.collect = !collect
                            homeAdapter.setData(position, data)
                            if (collect) {
                                viewModel.cancelCollectArticle(data.id)
                            } else {
                                viewModel.addCollectArticle(data.id)
                            }
                        } else {
                            Intent(activity, LoginActivity::class.java).run {
                                startActivity(this)
                            }
                            showToast(resources.getString(R.string.login_tint))
                        }
                    }
                }
            }
        }

}