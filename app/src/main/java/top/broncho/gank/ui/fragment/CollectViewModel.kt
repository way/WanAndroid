package top.broncho.gank.ui.fragment

import android.app.Application
import androidx.lifecycle.MutableLiveData
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.BaseListResponseBody
import top.broncho.gank.bean.CollectionArticle
import top.broncho.gank.http.exception.ErrorStatus
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class CollectViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }

    val article = MutableLiveData<Result<BaseListResponseBody<CollectionArticle>>>()
    val removeCollect = MutableLiveData<Result<Any>>()

    fun getCollectList(page: Int) = launchOnlyResult(
        block = {
            api.getCollectList(page)
        },
        success = {
            article.postValue(Result.success(it))
        },
        error = {
            article.postValue(Result.failure(it))
        }
    )

    fun removeCollectArticle(id: Int, originId: Int) = launchOnlyResult(
        block = {
            api.removeCollectArticle(id, originId)
        },
        success = {
            removeCollect.postValue(Result.success(it))
        },
        error = {
            removeCollect.postValue(Result.failure(it))
        }
    )

}