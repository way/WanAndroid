package top.broncho.gank.ui.fragment

import android.app.Application
import androidx.lifecycle.MutableLiveData
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.KnowledgeTreeBody
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class KnowledgeTreeViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }

    val article = MutableLiveData<Result<List<KnowledgeTreeBody>>>()

    fun requestKnowledgeTree() = launchOnlyResult(
        block = {
            api.getKnowledgeTree()
        },
        success = {
            article.postValue(Result.success(it))
        },
        error = {
            article.postValue(Result.failure(it))
        }
    )

}