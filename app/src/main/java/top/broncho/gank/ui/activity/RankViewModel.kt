package top.broncho.gank.ui.activity

import android.app.Application
import androidx.lifecycle.MutableLiveData
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.BaseListResponseBody
import top.broncho.gank.bean.CoinInfoBean
import top.broncho.gank.http.exception.ErrorStatus
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class RankViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }
    val rank = MutableLiveData<Result<BaseListResponseBody<CoinInfoBean>>>()

    fun getRankList(page: Int) = launchOnlyResult(
        block = {
            api.getRankList(page)
        },
        success = {
            rank.postValue(Result.success(it))
        },
        error = {
            rank.postValue(Result.failure(it))
        }
    )

}