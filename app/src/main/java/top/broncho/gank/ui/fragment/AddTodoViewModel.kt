package top.broncho.gank.ui.fragment

import android.app.Application
import androidx.lifecycle.MutableLiveData
import top.broncho.gank.api.ApiService
import top.broncho.gank.http.exception.ErrorStatus
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class AddTodoViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }

    val addTodo = MutableLiveData<Result<Any>>()
    val updateTodo = MutableLiveData<Result<Any>>()


    fun addTodo(map: MutableMap<String, Any>) = launchOnlyResult(
        block = {
            api.addTodo(map)
        },
        success = {
            addTodo.postValue(Result.success(it))
        },
        error = {
            addTodo.postValue(Result.failure(it))
        }
    )

    fun updateTodo(id: Int, map: MutableMap<String, Any>) = launchOnlyResult(
        block = {
            api.updateTodo(id, map)
        },
        success = {
            updateTodo.postValue(Result.success(it))
        },
        error = {
            updateTodo.postValue(Result.failure(it))
        }
    )
}