package top.broncho.gank.ui.fragment

import android.app.Application
import androidx.lifecycle.MutableLiveData
import top.broncho.gank.api.ApiService
import top.broncho.gank.http.exception.ErrorStatus
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class ShareArticleViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }

    val shareArticle = MutableLiveData<Result<Any>>()

    fun shareArticle(map: MutableMap<String, Any>) = launchOnlyResult(
        block = {
            api.shareArticle(map)
        },
        success = {
            shareArticle.postValue(Result.success(it))
        },
        error = {
            shareArticle.postValue(Result.failure(it))
        }
    )

}