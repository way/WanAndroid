package top.broncho.gank.ui.fragment

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.View
import kotlinx.android.synthetic.main.fragment_about.*
import top.broncho.gank.R
import top.broncho.gank.databinding.FragmentAboutBinding
import top.broncho.gank.utils.SettingUtil
import top.broncho.mvvm.base.BaseDiFragment

/**
 * Created by chenxz on 2018/6/10.
 */
class AboutFragment : BaseDiFragment<FragmentAboutBinding>() {

    companion object {
        fun getInstance(bundle: Bundle): AboutFragment {
            val fragment = AboutFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    fun initView(view: View) {
        about_content.run {
            text = Html.fromHtml(getString(R.string.about_content))
            movementMethod = LinkMovementMethod.getInstance()
        }

        val versionStr =
            getString(R.string.app_name) + " V" + activity?.packageManager?.getPackageInfo(
                activity?.packageName,
                0
            )?.versionName
        about_version.text = versionStr

        setLogoBg()

    }

    private fun setLogoBg() {
        val drawable = iv_logo.background as GradientDrawable
        drawable.setColor(SettingUtil.getColor())
        iv_logo.setBackgroundDrawable(drawable)
    }

    override fun getLayoutId(): Int = R.layout.fragment_about

}