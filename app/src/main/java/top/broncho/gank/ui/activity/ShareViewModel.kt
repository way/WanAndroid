package top.broncho.gank.ui.activity

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.ShareResponseBody
import top.broncho.gank.http.exception.ErrorStatus
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class ShareViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }

    val shareList = MutableLiveData<Result<ShareResponseBody>>()
    val deleteShareArticle = MutableLiveData<Result<Any>>()
    val collect = MutableLiveData<Result<Boolean>>()
    val cancel = MutableLiveData<Result<Boolean>>()

    fun getShareList(page: Int) = launchOnlyResult(
        block = {
            api.getShareList(page)
        },
        success = {
            shareList.postValue(Result.success(it))
        },
        error = {
            shareList.postValue(Result.failure(it))
        }
    )

    fun deleteShareArticle(id: Int) = launchOnlyResult(
        block = {
            api.deleteShareArticle(id)
        },
        success = {
            deleteShareArticle.postValue(Result.success(it))
        },
        error = {
            deleteShareArticle.postValue(Result.failure(it))
        }
    )

    fun cancelCollectArticle(id: Int) = launchResult(
        block = {
            api.cancelCollectArticle(id)
        },
        success = {
            cancel.postValue(Result.success(true))
        },
        error = {
            cancel.postValue(Result.failure(it))
        }
    )

    fun addCollectArticle(id: Int) = launchResult(
        block = {
            api.addCollectArticle(id)
        },
        success = {
            collect.postValue(Result.success(true))
        },
        error = {
            collect.postValue(Result.failure(it))
        }
    )

}