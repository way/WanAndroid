package top.broncho.gank.ui.fragment

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.NavigationBean
import top.broncho.gank.http.exception.ErrorStatus
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class NavigationViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }

    val article = MutableLiveData<Result<List<NavigationBean>>>()

    fun requestNavigationList() = launchOnlyResult(
        block = {
            api.getNavigationList()
        },
        success = {
            article.postValue(Result.success(it))
        },
        error = {
            article.postValue(Result.failure(it))
        }
    )

}