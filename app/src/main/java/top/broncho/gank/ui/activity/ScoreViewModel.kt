package top.broncho.gank.ui.activity

import android.app.Application
import androidx.lifecycle.MutableLiveData
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.BaseListResponseBody
import top.broncho.gank.bean.UserScoreBean
import top.broncho.gank.http.exception.ErrorStatus
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class ScoreViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }
    val scoreData = MutableLiveData<Result<BaseListResponseBody<UserScoreBean>>>()

    fun getUserScoreList(page: Int) = launchOnlyResult(
        block = {
            api.getUserScoreList(page)
        },
        success = {
            scoreData.postValue(Result.success(it))
        },
        error = {
            scoreData.postValue(Result.failure(it))
        }
    )

}