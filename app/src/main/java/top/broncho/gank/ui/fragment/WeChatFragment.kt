package top.broncho.gank.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import top.broncho.gank.R
import top.broncho.gank.adapter.WeChatPagerAdapter
import top.broncho.gank.bean.WXChapterBean
import top.broncho.gank.databinding.FragmentWechatBinding
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_wechat.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import top.broncho.mvvm.base.BaseDiFragment

/**
 * @author chenxz
 * @date 2018/10/28
 * @desc 公众号
 */
class WeChatFragment : BaseDiFragment<FragmentWechatBinding>() {

    companion object {
        fun getInstance(): WeChatFragment = WeChatFragment()
    }

    private val viewModel by viewModels<WeChatViewModel> { viewModelFactory }

    /**
     * datas
     */
    private val datas = mutableListOf<WXChapterBean>()

    /**
     * ViewPagerAdapter
     */
    private val viewPagerAdapter: WeChatPagerAdapter by lazy {
        WeChatPagerAdapter(datas, childFragmentManager)
    }

    override fun getLayoutId(): Int = R.layout.fragment_wechat
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        viewModel.article.observe({ lifecycle }) {
            if (it.isFailure) {
                return@observe
            }
            showWXChapters(it.getOrThrow())
        }
    }

    fun initView(view: View) {
        mLayoutStatusView = multiple_status_view
        viewPager.run {
            addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        }

        tabLayout.run {
            setupWithViewPager(viewPager)
            // TabLayoutHelper.setUpIndicatorWidth(tabLayout)
            addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewPager))
            addOnTabSelectedListener(onTabSelectedListener)
        }
    }

    fun showLoading() {
        mLayoutStatusView?.showLoading()
    }

    fun showError(errorMsg: String) {
        mLayoutStatusView?.showError()
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.article.value == null) {
            lazyLoad()
        }
    }

    fun lazyLoad() {
        viewModel.getWXChapters()
    }

    fun showWXChapters(chapters: MutableList<WXChapterBean>) {
        chapters.let {
            datas.addAll(it)
            doAsync {
                Thread.sleep(10)
                uiThread {
                    viewPager.run {
                        adapter = viewPagerAdapter
                        offscreenPageLimit = datas.size
                    }
                }
            }
        }
        if (chapters.isEmpty()) {
            mLayoutStatusView?.showEmpty()
        } else {
            mLayoutStatusView?.showContent()
        }
    }

    /**
     * onTabSelectedListener
     */
    private val onTabSelectedListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabReselected(tab: TabLayout.Tab?) {
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            // 默认切换的时候，会有一个过渡动画，设为false后，取消动画，直接显示
            tab?.let {
                viewPager.setCurrentItem(it.position, false)
            }
        }
    }

    fun scrollToTop() {
        if (viewPagerAdapter.count == 0) {
            return
        }
        val fragment: KnowledgeFragment =
            viewPagerAdapter.getItem(viewPager.currentItem) as KnowledgeFragment
        fragment.scrollToTop()
    }

}