package top.broncho.gank.ui.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import top.broncho.gank.R
import top.broncho.gank.constant.Constant
import top.broncho.gank.databinding.ActivityCommonBinding
import top.broncho.gank.ui.fragment.*
import kotlinx.android.synthetic.main.toolbar.*
import top.broncho.mvvm.base.BaseDiActivity

class CommonActivity : BaseDiActivity<ActivityCommonBinding>() {

    private var mType = ""

    override fun getLayoutId(): Int = R.layout.activity_common


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    fun initView() {
        intent.extras?.let {
            mType = it.getString(Constant.TYPE_KEY, "")
            toolbar.run {
                title = getString(R.string.app_name)
                setSupportActionBar(this)
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
            }
            val fragment = when (mType) {
                Constant.Type.COLLECT_TYPE_KEY -> {
                    toolbar.title = getString(R.string.collect)
                    CollectFragment.getInstance(it)
                }
                Constant.Type.ABOUT_US_TYPE_KEY -> {
                    toolbar.title = getString(R.string.about_us)
                    AboutFragment.getInstance(it)
                }
                Constant.Type.SEARCH_TYPE_KEY -> {
                    toolbar.title = it.getString(Constant.SEARCH_KEY, "")
                    SearchListFragment.getInstance(it)
                }
                Constant.Type.ADD_TODO_TYPE_KEY -> {
                    toolbar.title = getString(R.string.add)
                    AddTodoFragment.getInstance(it)
                }
                Constant.Type.EDIT_TODO_TYPE_KEY -> {
                    toolbar.title = getString(R.string.edit)
                    AddTodoFragment.getInstance(it)
                }
                Constant.Type.SEE_TODO_TYPE_KEY -> {
                    toolbar.title = getString(R.string.see)
                    AddTodoFragment.getInstance(it)
                }
                Constant.Type.SHARE_ARTICLE_TYPE_KEY -> {
                    toolbar.title = getString(R.string.share_article)
                    ShareArticleFragment.getInstance()
                }
                Constant.Type.SCAN_QR_CODE_TYPE_KEY -> {
                    toolbar.title = getString(R.string.scan_code_download)
                    QrCodeFragment.getInstance()
                }
                else -> {
                    null
                }
            }
            fragment ?: return
            supportFragmentManager.beginTransaction()
                .replace(R.id.common_frame_layout, fragment as Fragment, Constant.Type.COLLECT_TYPE_KEY)
                .commit()
        }
    }

    fun initColor() {
        //EventBus.getDefault().post(ColorEvent(true, mThemeColor))
    }

}
