package top.broncho.gank.ui.fragment

import android.app.Application
import androidx.lifecycle.MutableLiveData
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.Article
import top.broncho.gank.bean.ArticleResponseBody
import top.broncho.gank.bean.Banner
import top.broncho.gank.http.exception.ErrorStatus
import top.broncho.gank.utils.SettingUtil
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class HomeViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }

    //    val banner = liveData {
//        try {
//            val response = api.getBanners()
//            if (response.errorCode == ErrorStatus.SUCCESS) {
//                emit(Result.success(response.data))
//            } else {
//                emit(Result.failure(Throwable(response.errorMsg)))
//            }
//        } catch (e: Exception) {
//            emit(Result.failure(e))
//        }
//    }
    val banner = MutableLiveData<Result<List<Banner>>>()
    val article = MutableLiveData<Result<ArticleResponseBody>>()
    val collect = MutableLiveData<Result<Boolean>>()
    val cancel = MutableLiveData<Result<Boolean>>()

    fun requestArticles(num: Int) = launchOnlyResult(
        block = { api.getArticles(num) },
        success = { article.postValue(Result.success(it)) },
        error = { article.postValue(Result.failure(it)) }
    )

    fun cancelCollectArticle(id: Int) = launchResult(
        block = {
            api.cancelCollectArticle(id)
        },
        success = {
            cancel.postValue(Result.success(true))
        },
        error = {
            cancel.postValue(Result.failure(it))
        }
    )

    fun addCollectArticle(id: Int) = launchResult(
        block = {
            api.addCollectArticle(id)
        },
        success = {
            collect.postValue(Result.success(true))
        },
        error = {
            collect.postValue(Result.failure(it))
        }
    )

    fun requestHomeData() = launchUI {
        requestBanner()
        if (!SettingUtil.isHideTopArticle()) {
            try {
                val list = ArrayList<Article>()
                val top = api.getTopArticles()
                if (top.errorCode == ErrorStatus.SUCCESS) {
                    top.data.forEach { it.top = "1" }
                    list.addAll(top.data)
                }
                val response = api.getArticles(0)
                if (response.errorCode == ErrorStatus.SUCCESS) {
                    response.data.datas.addAll(0, list)
                    article.postValue(Result.success(response.data))
                } else {
                    article.postValue(Result.failure(Throwable(response.errorMsg)))
                }
            } catch (e: Exception) {
                article.postValue(Result.failure(e))
            }
        } else {
            try {
                val response = api.getArticles(0)
                if (response.errorCode == ErrorStatus.SUCCESS) {
                    article.postValue(Result.success(response.data))
                } else {
                    article.postValue(Result.failure(Throwable(response.errorMsg)))
                }
            } catch (e: Exception) {
                article.postValue(Result.failure(e))
            }
        }
    }

    private suspend fun requestBanner() {
        try {
            val response = api.getBanners()
            if (response.errorCode == ErrorStatus.SUCCESS) {
                banner.postValue(Result.success(response.data))
            } else {
                banner.postValue(Result.failure(Throwable(response.errorMsg)))
            }
        } catch (e: Exception) {
            banner.postValue(Result.failure(e))
        }
    }
}