package top.broncho.gank.ui.fragment

import top.broncho.gank.R
import top.broncho.gank.databinding.FragmentQrCodeBinding
import top.broncho.mvvm.base.BaseDiFragment

/**
 * @author chenxz
 * @date 2019/11/17
 * @desc 扫码下载
 */
class QrCodeFragment : BaseDiFragment<FragmentQrCodeBinding>() {

    companion object {
        fun getInstance(): QrCodeFragment = QrCodeFragment()
    }

    override fun getLayoutId(): Int = R.layout.fragment_qr_code

}