package top.broncho.gank.ui.activity

import android.app.Application
import androidx.lifecycle.MutableLiveData
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.UserInfoBody
import top.broncho.gank.http.exception.ErrorStatus
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }
    val logoutStatus = MutableLiveData<Result<Any>>()
    val userInfo = MutableLiveData<Result<UserInfoBody>>()

    fun logout() = launchResult(
        block = {
            api.logout()
        },
        success = {
            logoutStatus.postValue(Result.success(true))
        },
        error = {
            logoutStatus.postValue(Result.failure(it))
        }
    )

    fun getUserInfo() = launchOnlyResult(
        block = {
            api.getUserInfo()
        },
        success = {
            userInfo.postValue(Result.success(it))
        },
        error = {
            userInfo.postValue(Result.failure(it))
        }
    )

}