package top.broncho.gank.ui.activity

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.LoginData
import top.broncho.gank.http.exception.ErrorStatus
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class LoginViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }
    val login = MutableLiveData<Result<LoginData>>()

    fun loginWanAndroid(username: String, password: String) = launchOnlyResult(
        block = {
            api.loginWanAndroid(username, password)
        },
        success = {
            login.postValue(Result.success(it))
        },
        error = {
            login.postValue(Result.failure(it))
        }
    )
}