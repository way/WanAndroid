package top.broncho.gank.ui.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.chad.library.adapter.base.BaseQuickAdapter
import com.cxz.multiplestatusview.MultipleStatusView
import top.broncho.gank.R
import top.broncho.gank.adapter.RankAdapter
import top.broncho.gank.bean.BaseListResponseBody
import top.broncho.gank.bean.CoinInfoBean
import top.broncho.gank.databinding.ActivityRankBinding
import top.broncho.gank.widget.SpaceItemDecoration
import kotlinx.android.synthetic.main.fragment_refresh_layout.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.warn
import top.broncho.mvvm.base.BaseVMActivity

/**
 * 排行榜页面
 */
class RankActivity : BaseVMActivity<ActivityRankBinding>() {
    private val viewModel by viewModels<RankViewModel> { viewModelFactory }
    /**
     * 每页数据的个数
     */
    private var pageSize = 20;

    /**
     * RecyclerView Divider
     */
    private val recyclerViewItemDecoration by lazy {
        SpaceItemDecoration(this)
    }

    private val rankAdapter: RankAdapter by lazy {
        RankAdapter()
    }

    /**
     * is Refresh
     */
    private var isRefresh = true
    private var mLayoutStatusView: MultipleStatusView? = null


    override fun getLayoutId(): Int = R.layout.activity_rank

    fun showLoading() {
        // swipeRefreshLayout.isRefreshing = isRefresh
    }

    fun hideLoading() {
        swipeRefreshLayout?.isRefreshing = false
        if (isRefresh) {
            rankAdapter.setEnableLoadMore(true)
        }
    }

    fun showError(errorMsg: String) {
        mLayoutStatusView?.showError()
        if (isRefresh) {
            rankAdapter.setEnableLoadMore(true)
        } else {
            rankAdapter.loadMoreFail()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        viewModel.rank.observe({ lifecycle }) {
            swipeRefreshLayout.isRefreshing = false
            if (it.isFailure) {
                warn { "login failed:${it.exceptionOrNull()}" }
                showError(it.exceptionOrNull().toString())
                return@observe
            }
            mLayoutStatusView?.showContent()
            showRankList(it.getOrThrow())
        }
    }

    fun initView() {
        mLayoutStatusView = multiple_status_view
        toolbar.run {
            title = getString(R.string.score_list)
            setSupportActionBar(this)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        swipeRefreshLayout.run {
            setOnRefreshListener(onRefreshListener)
        }
        recyclerView.run {
            layoutManager =
                LinearLayoutManager(this@RankActivity)
            adapter = rankAdapter
            itemAnimator = DefaultItemAnimator()
            addItemDecoration(recyclerViewItemDecoration)
        }
        rankAdapter.run {
            bindToRecyclerView(recyclerView)
            setOnLoadMoreListener(onRequestLoadMoreListener, recyclerView)
        }
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.rank.value == null) {
            start()
        }
    }

    fun start() {
        mLayoutStatusView?.showLoading()
        viewModel.getRankList(1)
    }

    fun showRankList(body: BaseListResponseBody<CoinInfoBean>) {
        body.datas.let {
            rankAdapter.run {
                if (isRefresh) {
                    replaceData(it)
                } else {
                    addData(it)
                }
                pageSize = body.size
                if (body.over) {
                    loadMoreEnd(isRefresh)
                } else {
                    loadMoreComplete()
                }
            }
        }
        if (rankAdapter.data.isEmpty()) {
            mLayoutStatusView?.showEmpty()
        } else {
            mLayoutStatusView?.showContent()
        }
    }

    /**
     * RefreshListener
     */
    private val onRefreshListener = SwipeRefreshLayout.OnRefreshListener {
        isRefresh = true
        rankAdapter.setEnableLoadMore(false)
        viewModel.getRankList(1)
    }

    /**
     * LoadMoreListener
     */
    private val onRequestLoadMoreListener = BaseQuickAdapter.RequestLoadMoreListener {
        isRefresh = false
        swipeRefreshLayout.isRefreshing = false
        val page = rankAdapter.data.size / pageSize + 1
        viewModel.getRankList(page)
    }

}
