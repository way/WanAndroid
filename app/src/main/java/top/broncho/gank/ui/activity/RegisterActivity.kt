package top.broncho.gank.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import top.broncho.gank.R
import top.broncho.gank.bean.LoginData
import top.broncho.gank.constant.Constant
import top.broncho.gank.databinding.ActivityRegisterBinding
import top.broncho.gank.ext.showToast
import top.broncho.gank.utils.DialogUtil
import top.broncho.gank.utils.Preference
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.warn
import top.broncho.mvvm.base.BaseVMActivity

class RegisterActivity : BaseVMActivity<ActivityRegisterBinding>() {
    private val viewModel by viewModels<RegisterViewModel> { viewModelFactory }
    /**
     * local username
     */
    private var user: String by Preference(Constant.USERNAME_KEY, "")

    /**
     * local password
     */
    private var pwd: String by Preference(Constant.PASSWORD_KEY, "")

    private val mDialog by lazy {
        DialogUtil.getWaitDialog(this, getString(R.string.register_ing))
    }

    fun showLoading() {
        mDialog.show()
    }

    fun hideLoading() {
        mDialog.dismiss()
    }

    fun registerSuccess(data: LoginData) {
        showToast(getString(R.string.register_success))
        isLogin = true
        user = data.username
        pwd = data.password

        finish()
    }

    fun registerFail() {
        isLogin = false
    }

    override fun getLayoutId(): Int = R.layout.activity_register

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        viewModel.register.observe({ lifecycle }) {
            hideLoading()
            if (it.isFailure) {
                warn { "login failed:${it.exceptionOrNull()}" }
                registerFail()
                return@observe
            }
            registerSuccess(it.getOrThrow())
        }
    }

    fun initView() {
        toolbar.run {
            title = resources.getString(R.string.register)
            setSupportActionBar(this)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        btn_register.setOnClickListener(onClickListener)
        tv_sign_in.setOnClickListener(onClickListener)
    }

    /**
     * OnClickListener
     */
    private val onClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.btn_register -> {
                register()
            }
            R.id.tv_sign_in -> {
                Intent(this@RegisterActivity, LoginActivity::class.java).apply {
                    startActivity(this)
                }
                finish()
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            }
        }
    }

    /**
     * Register
     */
    private fun register() {
        if (validate()) {
            showLoading()
            viewModel.registerWanAndroid(
                et_username.text.toString(),
                et_password.text.toString(),
                et_password2.text.toString()
            )
        }
    }

    /**
     * check data
     */
    private fun validate(): Boolean {
        var valid = true
        val username: String = et_username.text.toString()
        val password: String = et_password.text.toString()
        val password2: String = et_password2.text.toString()
        if (username.isEmpty()) {
            et_username.error = getString(R.string.username_not_empty)
            valid = false
        }
        if (password.isEmpty()) {
            et_password.error = getString(R.string.password_not_empty)
            valid = false
        }
        if (password2.isEmpty()) {
            et_password2.error = getString(R.string.confirm_password_not_empty)
            valid = false
        }
        if (password != password2) {
            et_password2.error = getString(R.string.password_cannot_match)
            valid = false
        }
        return valid
    }

}
