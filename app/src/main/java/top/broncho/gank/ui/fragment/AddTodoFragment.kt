package top.broncho.gank.ui.fragment

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import top.broncho.gank.R
import top.broncho.gank.bean.TodoBean
import top.broncho.gank.constant.Constant
import top.broncho.gank.databinding.FragmentAddTodoBinding
import top.broncho.gank.ext.formatCurrentDate
import top.broncho.gank.ext.showToast
import top.broncho.gank.ext.stringToCalendar
import top.broncho.gank.utils.DialogUtil
import kotlinx.android.synthetic.main.fragment_add_todo.*
import top.broncho.mvvm.base.BaseDiFragment
import java.util.*

/**
 * Created by chenxz on 2018/8/11.
 */
class AddTodoFragment : BaseDiFragment<FragmentAddTodoBinding>() {

    companion object {
        fun getInstance(bundle: Bundle): AddTodoFragment {
            val fragment = AddTodoFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private val viewModel by viewModels<AddTodoViewModel> { viewModelFactory }

    /**
     * Date
     */
    private var mCurrentDate = formatCurrentDate()

    /**
     * 类型
     */
    private var mType: Int = 0
    private var mTodoBean: TodoBean? = null

    /**
     * 新增，编辑，查看 三种状态
     */
    private var mTypeKey = ""

    /**
     * id
     */
    private var mId: Int? = 0

    /**
     * 优先级  重要（1），一般（0）
     */
    private var mPriority = 0

    private val mDialog by lazy {
        DialogUtil.getWaitDialog(activity!!, getString(R.string.save_ing))
    }

    fun showLoading() {
        mDialog.show()
    }

    fun hideLoading() {
        mDialog.dismiss()
    }

    override fun getLayoutId(): Int = R.layout.fragment_add_todo

    fun getType(): Int = mType
    fun getCurrentDate(): String = tv_date.text.toString()
    fun getTitle(): String = et_title.text.toString()
    fun getContent(): String = et_content.text.toString()
    fun getStatus(): Int = mTodoBean?.status ?: 0
    fun getItemId(): Int = mTodoBean?.id ?: 0
    fun getPriority(): String = mPriority.toString()
    private fun getTodo(): MutableMap<String, Any> {
        val type = getType()
        val title = getTitle()
        val content = getContent()
        val date = getCurrentDate()
        val status = getStatus()
        val priority = getPriority()

        val map = mutableMapOf<String, Any>()
        map["type"] = type
        map["title"] = title
        map["content"] = content
        map["date"] = date
        map["status"] = status
        map["priority"] = priority
        return map
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        viewModel.addTodo.observe({ lifecycle }) {
            showAddTodo(it.isSuccess)
        }
        viewModel.updateTodo.observe({ lifecycle }) {
            showUpdateTodo(it.isSuccess)
        }
    }

    fun initView(view: View) {

        mType = arguments?.getInt(Constant.TODO_TYPE) ?: 0
        mTypeKey = arguments?.getString(Constant.TYPE_KEY) ?: Constant.Type.ADD_TODO_TYPE_KEY

        when (mTypeKey) {
            Constant.Type.ADD_TODO_TYPE_KEY -> {
                tv_date.text = formatCurrentDate()
            }
            Constant.Type.EDIT_TODO_TYPE_KEY -> {
                mTodoBean = arguments?.getSerializable(Constant.TODO_BEAN) as TodoBean ?: null
                et_title.setText(mTodoBean?.title)
                et_content.setText(mTodoBean?.content)
                tv_date.text = mTodoBean?.dateStr
                mPriority = mTodoBean?.priority ?: 0
                if (mTodoBean?.priority == 0) {
                    rb0.isChecked = true
                    rb1.isChecked = false
                } else if (mTodoBean?.priority == 1) {
                    rb0.isChecked = false
                    rb1.isChecked = true
                }
            }
            Constant.Type.SEE_TODO_TYPE_KEY -> {
                mTodoBean = arguments?.getSerializable(Constant.TODO_BEAN) as TodoBean ?: null
                et_title.setText(mTodoBean?.title)
                et_content.setText(mTodoBean?.content)
                tv_date.text = mTodoBean?.dateStr
                et_title.isEnabled = false
                et_content.isEnabled = false
                ll_date.isEnabled = false
                btn_save.visibility = View.GONE
                iv_arrow_right.visibility = View.GONE

                ll_priority.isEnabled = false
                if (mTodoBean?.priority == 0) {
                    rb0.isChecked = true
                    rb1.isChecked = false
                    rb1.visibility = View.GONE
                } else if (mTodoBean?.priority == 1) {
                    rb0.isChecked = false
                    rb1.isChecked = true
                    rb0.visibility = View.GONE
                } else {
                    ll_priority.visibility = View.GONE
                }
            }
        }

        ll_date.setOnClickListener {
            var now = Calendar.getInstance()
            if (mTypeKey == Constant.Type.EDIT_TODO_TYPE_KEY) {
                mTodoBean?.dateStr?.let {
                    now = it.stringToCalendar()
                }
            }
            val dpd = DatePickerDialog(
                requireActivity(),
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    mCurrentDate = "$year-${month + 1}-$dayOfMonth"
                    tv_date.text = mCurrentDate
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
            )
            dpd.show()
        }

        btn_save.setOnClickListener {
            when (mTypeKey) {
                Constant.Type.ADD_TODO_TYPE_KEY -> {
                    viewModel.addTodo(getTodo())
                }
                Constant.Type.EDIT_TODO_TYPE_KEY -> {
                    viewModel.updateTodo(getItemId(), getTodo())
                }
            }
        }

        rg_priority.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.rb0) {
                mPriority = 0
                rb0.isChecked = true
                rb1.isChecked = false
            } else if (checkedId == R.id.rb1) {
                mPriority = 1
                rb0.isChecked = false
                rb1.isChecked = true
            }
        }

    }

    fun showAddTodo(success: Boolean) {
        if (success) {
            showToast(getString(R.string.save_success))
//            EventBus.getDefault().post(RefreshTodoEvent(true, mType))
            activity?.finish()
        }
    }

    fun showUpdateTodo(success: Boolean) {
        if (success) {
            showToast(getString(R.string.save_success))
//            EventBus.getDefault().post(RefreshTodoEvent(true, mType))
            activity?.finish()
        }
    }

}