package top.broncho.gank.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import top.broncho.gank.R
import top.broncho.gank.adapter.ProjectPagerAdapter
import top.broncho.gank.bean.ProjectTreeBean
import top.broncho.gank.databinding.FragmentProjectBinding
import top.broncho.gank.utils.SettingUtil
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_project.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import top.broncho.mvvm.base.BaseDiFragment

/**
 * Created by chenxz on 2018/5/15.
 */
class ProjectFragment : BaseDiFragment<FragmentProjectBinding>() {

    companion object {
        fun getInstance(): ProjectFragment = ProjectFragment()
    }

    private val viewModel by viewModels<ProjectViewModel> { viewModelFactory }

    override fun getLayoutId(): Int = R.layout.fragment_project

    /**
     * ProjectTreeBean
     */
    private var projectTree = mutableListOf<ProjectTreeBean>()

    /**
     * ViewPagerAdapter
     */
    private val viewPagerAdapter: ProjectPagerAdapter by lazy {
        ProjectPagerAdapter(projectTree, childFragmentManager)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        viewModel.article.observe({ lifecycle }) {
            if (it.isFailure) {
                return@observe
            }
            setProjectTree(it.getOrThrow())
        }
    }

    fun initView(view: View) {
        mLayoutStatusView = multiple_status_view
        viewPager.run {
            addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        }

        tabLayout.run {
            setupWithViewPager(viewPager)
            // TabLayoutHelper.setUpIndicatorWidth(tabLayout)
            addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewPager))
            addOnTabSelectedListener(onTabSelectedListener)
        }
    }

    fun showLoading() {
        mLayoutStatusView?.showLoading()
    }

    fun showError(errorMsg: String) {
        mLayoutStatusView?.showError()
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.article.value == null) {
            lazyLoad()
        }
    }

    fun lazyLoad() {
        viewModel.requestProjectTree()
    }

    fun doReConnected() {
        if (projectTree.size == 0) {
        }
    }

    fun setProjectTree(list: List<ProjectTreeBean>) {
        list.let {
            projectTree.addAll(it)
            doAsync {
                Thread.sleep(10)
                uiThread {
                    viewPager.run {
                        adapter = viewPagerAdapter
                        offscreenPageLimit = projectTree.size
                    }
                }
            }
        }
        if (list.isEmpty()) {
            mLayoutStatusView?.showEmpty()
        } else {
            mLayoutStatusView?.showContent()
        }
    }

    /**
     * onTabSelectedListener
     */
    private val onTabSelectedListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabReselected(tab: TabLayout.Tab?) {
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            // 默认切换的时候，会有一个过渡动画，设为false后，取消动画，直接显示
            tab?.let {
                viewPager.setCurrentItem(it.position, false)
            }
        }
    }

    fun scrollToTop() {
        if (viewPagerAdapter.count == 0) {
            return
        }
        val fragment: ProjectListFragment =
            viewPagerAdapter.getItem(viewPager.currentItem) as ProjectListFragment
        fragment.scrollToTop()
    }

}