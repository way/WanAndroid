package top.broncho.gank.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import top.broncho.gank.R
import top.broncho.gank.databinding.FragmentSystemBinding
import top.broncho.gank.utils.SettingUtil
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_system.*
import top.broncho.mvvm.base.BaseDiFragment

/**
 * @author chenxz
 * @date 2019/11/17
 * @desc 体系
 */
class SystemFragment : BaseDiFragment<FragmentSystemBinding>() {

    companion object {
        fun getInstance(): SystemFragment = SystemFragment()
    }

    private val titleList = mutableListOf<String>()
    private val fragmentList = mutableListOf<Fragment>()
    private val systemPagerAdapter: SystemPagerAdapter by lazy {
        SystemPagerAdapter(childFragmentManager, titleList, fragmentList)
    }

    override fun getLayoutId(): Int = R.layout.fragment_system
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    fun initView(view: View) {
        titleList.add(getString(R.string.knowledge_system))
        titleList.add(getString(R.string.navigation))
        fragmentList.add(KnowledgeTreeFragment.getInstance())
        fragmentList.add(NavigationFragment.getInstance())

        viewPager.run {
            addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
            adapter = systemPagerAdapter
        }

        tabLayout.run {
            setupWithViewPager(viewPager)
            addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewPager))
            addOnTabSelectedListener(onTabSelectedListener)
        }
    }

    fun scrollToTop() {
        if (viewPager.currentItem == 0) {
            (systemPagerAdapter.getItem(0) as KnowledgeTreeFragment).scrollToTop()
        } else if (viewPager.currentItem == 1) {
            (systemPagerAdapter.getItem(1) as NavigationFragment).scrollToTop()
        }
    }

    /**
     * onTabSelectedListener
     */
    private val onTabSelectedListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabReselected(tab: TabLayout.Tab?) {
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            // 默认切换的时候，会有一个过渡动画，设为false后，取消动画，直接显示
            tab?.let {
                viewPager.setCurrentItem(it.position, false)
            }
        }
    }

    class SystemPagerAdapter(
        fm: FragmentManager,
        private val titleList: MutableList<String>,
        private val fragmentList: MutableList<Fragment>
    ) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getItem(i: Int): Fragment = fragmentList[i]

        override fun getCount(): Int = fragmentList.size

        override fun getPageTitle(position: Int): CharSequence? = titleList[position]

    }

}