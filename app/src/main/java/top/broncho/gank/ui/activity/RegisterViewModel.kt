package top.broncho.gank.ui.activity

import android.app.Application
import androidx.lifecycle.MutableLiveData
import retrofit2.Retrofit
import retrofit2.create
import top.broncho.gank.api.ApiService
import top.broncho.gank.bean.LoginData
import top.broncho.mvvm.base.BaseViewModel
import javax.inject.Inject

class RegisterViewModel @Inject constructor(app: Application, private val retrofit: Retrofit) :
    BaseViewModel(app) {
    private val api by lazy { retrofit.create<ApiService>() }
    val register = MutableLiveData<Result<LoginData>>()

    fun registerWanAndroid(username: String, password: String, repassword: String) =
        launchOnlyResult(
            block = {
                api.registerWanAndroid(username, password, repassword)
            },
            success = {
                register.postValue(Result.success(it))
            },
            error = {
                register.postValue(Result.failure(it))
            }
        )

}