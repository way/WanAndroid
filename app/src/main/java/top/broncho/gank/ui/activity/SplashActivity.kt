package top.broncho.gank.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import top.broncho.gank.R
import top.broncho.gank.databinding.ActivitySplashBinding
import top.broncho.mvvm.base.BaseVMActivity

class SplashActivity : BaseVMActivity<ActivitySplashBinding>() {

    private var alphaAnimation: AlphaAnimation? = null

    override fun getLayoutId(): Int = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    fun initView() {
        alphaAnimation = AlphaAnimation(0.3F, 1.0F)
        alphaAnimation?.run {
            duration = 2000
            setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    jumpToMain()
                }

                override fun onAnimationStart(p0: Animation?) {
                }
            })
        }
        binding.layoutSplash.startAnimation(alphaAnimation)
    }

    override fun initColor() {
        super.initColor()
        binding.layoutSplash.setBackgroundColor(mThemeColor)
    }

    fun jumpToMain() {
        val intent = Intent(this, top.broncho.gank.ui.main.MainActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

}
